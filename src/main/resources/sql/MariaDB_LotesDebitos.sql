 select sum(importe) as total, count(*) as debitos, entidad, unidad, lote
  from debitos_enviados
  where enviado between ? AND ?
  group by entidad, unidad, lote
  order by entidad, unidad, lote
