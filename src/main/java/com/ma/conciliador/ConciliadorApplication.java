package com.ma.conciliador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConciliadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConciliadorApplication.class, args);
	}

}
