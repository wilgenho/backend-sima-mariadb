package com.ma.conciliador;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DBConfig {

    @Bean(name = "as400DataSource")
    @ConfigurationProperties(prefix="spring.ds-as400")
    public DataSource as400DataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "as400JdbcTemplate")
    public JdbcTemplate as400JdbcTemplate(
            @Qualifier("as400DataSource") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "mariaDbDataSource")
    @ConfigurationProperties(prefix="spring.ds-mariadb")
    public DataSource mariaDbDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "mariaDbJdbcTemplate")
    public JdbcTemplate mariaDbJdbcTemplate(
            @Qualifier("mariaDbDataSource") DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }

}
