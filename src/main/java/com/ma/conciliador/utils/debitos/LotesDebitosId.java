package com.ma.conciliador.utils.debitos;

import lombok.Value;

@Value
public class LotesDebitosId {
    Integer entidad;
    Integer unidad;
    Integer lote;
}
