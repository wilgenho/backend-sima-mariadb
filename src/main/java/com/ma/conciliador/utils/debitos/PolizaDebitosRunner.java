package com.ma.conciliador.utils.debitos;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;


@Log4j2
public class PolizaDebitosRunner implements CommandLineRunner {

    private final DebitosAS400Repository as400Repository;
    private final DebitosMariaDBRepository mariaDBRepository;

    public PolizaDebitosRunner(DebitosAS400Repository as400Repository, DebitosMariaDBRepository mariaDBRepository) {
        this.as400Repository = as400Repository;
        this.mariaDBRepository = mariaDBRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Map<Long, Conciliacion<Long>> map = getConciliacion();
        mariaDBRepository.getPolizas(1, 1, 260)
                .forEach(getPolizaDebitosConsumer(map));

        crearArchivo(map);
    }

    private void crearArchivo(Map<Long, Conciliacion<Long>> map) throws IOException {
        FileWriter out = new FileWriter("polizas.csv");

        CSVFormat format = CSVFormat.EXCEL.withDelimiter(';')
                .withHeader("poliza", "registros origen", "total origen",
                        "registros destino", "total destino", "diferencia registros", "diferencia totales");
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        try (CSVPrinter printer = new CSVPrinter(out, format)) {
            map.forEach((aLong, conciliacion) -> {
                try {
                    printer.printRecord(
                            conciliacion.getClave(),
                            conciliacion.getRegistrosOrigen(),
                            conciliacion.getRegistrosOrigen(),
                            decimalFormat.format(conciliacion.getTotalOrigen()),
                            conciliacion.getRegistrosDestino(),
                            decimalFormat.format(conciliacion.getTotalDestino()),
                            conciliacion.getDiferenciaRegistos(),
                            decimalFormat.format(conciliacion.getDiferenciaTotal()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private Consumer<PolizaDebitos> getPolizaDebitosConsumer(Map<Long, Conciliacion<Long>> map) {
        return polizaDebitos -> {
            Long id = polizaDebitos.getPoliza();
            if (map.containsKey(id)) {
                Conciliacion<Long> conciliacion = map.get(id);
                map.put(id, new Conciliacion<>(conciliacion.getClave(),
                        conciliacion.getRegistrosOrigen(),
                        conciliacion.getTotalOrigen(),
                        polizaDebitos.getCantidad().longValue(),
                        polizaDebitos.getImporte()));
            } else {
                map.put(id, new Conciliacion<>(polizaDebitos.getPoliza(),
                        0L, BigDecimal.ZERO,
                        polizaDebitos.getCantidad().longValue(),
                        polizaDebitos.getImporte()));
            }
        };
    }

    private Map<Long, Conciliacion<Long>> getConciliacion() {
        return  as400Repository.getPolizas(1, 1, 260)
                .stream()
                .collect(Collectors.toMap(PolizaDebitos::getPoliza, this::generarConciliacion));
    }

    private Conciliacion<Long> generarConciliacion(PolizaDebitos polizaDebitos) {
        return new Conciliacion<>(
                polizaDebitos.getPoliza(), polizaDebitos.getCantidad().longValue(),
                polizaDebitos.getImporte(), 0L, BigDecimal.ZERO);
    }
}
