package com.ma.conciliador.utils.debitos;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Prueba {

    public static void main(String[] args) {
        DecimalFormat dc = new DecimalFormat("#0.00");

        BigDecimal bc = new BigDecimal("-898123.44");
        System.out.println(dc.format(bc));
    }
}
