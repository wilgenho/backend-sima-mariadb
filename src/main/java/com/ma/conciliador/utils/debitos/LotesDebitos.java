package com.ma.conciliador.utils.debitos;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class LotesDebitos {

    Integer entidad;
    Integer unidad;
    Integer lote;
    Long cantidad;
    BigDecimal total;

    public Long getId () {
        return lote * 10000L + entidad * 100L + unidad;
    }

    public LotesDebitosId getClave() {
        return new LotesDebitosId(entidad, unidad, lote);
    }

    @Override
    public String toString() {
        return "LotesDebitos{" + getId() +"}";
    }
}
