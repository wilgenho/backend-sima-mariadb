package com.ma.conciliador.utils.debitos;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PolizaDebitosRowMapper implements RowMapper<PolizaDebitos> {
    @Override
    public PolizaDebitos mapRow(ResultSet rs, int i) throws SQLException {
        return new PolizaDebitos(
                rs.getLong("POLIZA"),
                rs.getBigDecimal("IMPORTE"),
                rs.getInt("CANTIDAD")
        );
    }
}
