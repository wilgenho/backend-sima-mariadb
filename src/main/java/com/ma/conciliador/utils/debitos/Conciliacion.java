package com.ma.conciliador.utils.debitos;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class Conciliacion<T> {

    T clave;
    Long registrosOrigen;
    BigDecimal totalOrigen;
    Long registrosDestino;
    BigDecimal totalDestino;

    public Long getDiferenciaRegistos() {
        return registrosOrigen - registrosDestino;
    }

    public BigDecimal getDiferenciaTotal() {
        return totalOrigen.subtract(totalDestino);
    }

    @Override
    public String toString() {
        return "Conciliacion{" +
                "clave='" + clave + '\'' +
                ", registrosOrigen=" + registrosOrigen +
                ", totalOrigen=" + totalOrigen +
                ", registrosDestino=" + registrosDestino +
                ", totalDestino=" + totalDestino +
                ", diferenciaTotal=" + getDiferenciaTotal() +
                ", diferenciaRegistros=" + getDiferenciaRegistos() +
                '}';
    }
}
