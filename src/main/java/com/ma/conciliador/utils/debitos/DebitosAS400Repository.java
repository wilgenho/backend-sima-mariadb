package com.ma.conciliador.utils.debitos;

import com.ma.conciliador.utils.FileReader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Repository
public class DebitosAS400Repository {

    private static final String QUERY_LOTES = "sql/AS400_LotesDebitos.sql";
    private static final String QUERY_POLIZAS = "sql/AS400_PolizasDebitos.sql";

    private final JdbcTemplate jdbcTemplate;

    public DebitosAS400Repository(@Qualifier("as400JdbcTemplate") final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<LotesDebitos> getLotes(LocalDate desde, LocalDate hasta) {
        String sql = FileReader.fromResource(QUERY_LOTES);
        return jdbcTemplate.query(sql, new LotesDebitosRowMapper(),
                Date.valueOf(desde), Date.valueOf(hasta));
    }

    public List<PolizaDebitos> getPolizas(Integer unidad, Integer entidad, Integer lote) {
        String sql = FileReader.fromResource(QUERY_POLIZAS);
        return jdbcTemplate.query(sql, new PolizaDebitosRowMapper(), unidad, entidad, lote);
    }

}
