package com.ma.conciliador.utils.debitos;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component
@Log4j2
public class LotesDebitosRunner implements CommandLineRunner {

    private final DebitosAS400Repository as400Repository;
    private final DebitosMariaDBRepository mariaDBRepository;

    public LotesDebitosRunner(DebitosAS400Repository as400Repository,
                              DebitosMariaDBRepository mariaDBRepository) {
        this.as400Repository = as400Repository;
        this.mariaDBRepository = mariaDBRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Map<Long, Conciliacion<LotesDebitosId>> map = getConciliacion();

        mariaDBRepository.getLotes(LocalDate.of(2021, 5, 1),
                LocalDate.of(2021, 5, 30))
                .forEach(getLotesDebitosConsumer(map));

        crearArchivo(map);
    }

    private void crearArchivo(Map<Long, Conciliacion<LotesDebitosId>> map) throws IOException {
        FileWriter out = new FileWriter("lotes.csv");

        CSVFormat format = CSVFormat.EXCEL.withDelimiter(';')
                .withHeader("unidad", "entidad", "lote", "registros origen", "total origen",
                        "registros destino", "total destino", "diferencia registros", "diferencia totales");
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        try (CSVPrinter printer = new CSVPrinter(out, format)) {
            map.forEach((aLong, conciliacion) -> {
                try {
                    printer.printRecord(
                            conciliacion.getClave().getUnidad(),
                            conciliacion.getClave().getEntidad(),
                            conciliacion.getClave().getLote(),
                            conciliacion.getRegistrosOrigen(),
                            decimalFormat.format(conciliacion.getTotalOrigen()),
                            conciliacion.getRegistrosDestino(),
                            decimalFormat.format(conciliacion.getTotalDestino()),
                            conciliacion.getDiferenciaRegistos(),
                            decimalFormat.format(conciliacion.getDiferenciaTotal()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private Consumer<LotesDebitos> getLotesDebitosConsumer(Map<Long, Conciliacion<LotesDebitosId>> map) {
        return lotesDebitos -> {
            Long id = lotesDebitos.getId();
            if (map.containsKey(id)) {
                Conciliacion<LotesDebitosId> conciliacion = map.get(id);
                map.put(id, new Conciliacion<>(conciliacion.getClave(),
                        conciliacion.getRegistrosOrigen(),
                        conciliacion.getTotalOrigen(),
                        lotesDebitos.getCantidad(),
                        lotesDebitos.getTotal()));
            } else {
                map.put(id, new Conciliacion<>(lotesDebitos.getClave(),
                        0L, BigDecimal.ZERO,
                        lotesDebitos.getCantidad(),
                        lotesDebitos.getTotal()));
            }
        };
    }

    private Map<Long, Conciliacion<LotesDebitosId>> getConciliacion() {
        return as400Repository.getLotes(LocalDate.of(2021, 5, 1),
                LocalDate.of(2021, 5, 30))
                .stream()
                .collect(Collectors.toMap(LotesDebitos::getId, this::generarConciliacion));
    }

    private Conciliacion<LotesDebitosId> generarConciliacion(LotesDebitos lote) {
        return new Conciliacion<>(lote.getClave(),
                lote.getCantidad(),
                lote.getTotal(), 0L, BigDecimal.ZERO);
    }


}
