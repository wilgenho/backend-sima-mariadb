package com.ma.conciliador.utils.debitos;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LotesDebitosRowMapper implements RowMapper<LotesDebitos> {
    @Override
    public LotesDebitos mapRow(ResultSet rs, int i) throws SQLException {
        return new LotesDebitos(
                rs.getInt("ENTIDAD"),
                rs.getInt("UNIDAD"),
                rs.getInt("LOTE"),
                rs.getLong("DEBITOS"),
                rs.getBigDecimal("TOTAL"));
    }
}
