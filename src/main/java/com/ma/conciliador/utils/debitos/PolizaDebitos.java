package com.ma.conciliador.utils.debitos;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class PolizaDebitos {
    Long poliza;
    BigDecimal importe;
    Integer cantidad;
}
