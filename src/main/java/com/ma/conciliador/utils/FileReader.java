package com.ma.conciliador.utils;

import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class FileReader {

    public static String fromResource(String path) {
        String texto;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new ClassPathResource(path).getInputStream(), StandardCharsets.UTF_8))) {
            texto = reader.lines().collect(Collectors.joining(" "));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return texto;
    }

}
